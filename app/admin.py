from django.contrib import admin
from app.models import School, Course, Company, Background, Project, Skill

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'zip_code', 'address', 'website_url', 'description', 'logo', 'status')
    list_filter = ('name', )
    search_fields = ('name', )

@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('school', 'diploma', 'start_date', 'end_date', 'course_url', 'description')
    list_filter = ('school', 'diploma')
    search_fields = ('school', 'diploma')

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'zip_code', 'address', 'website_url', 'description', 'logo', 'activity')
    list_filter = ('name', )
    search_fields = ('name', )

@admin.register(Background)
class BackgroundAdmin(admin.ModelAdmin):
    list_display = ('company', 'position_held', 'employment_contract', 'start_date', 'end_date', 'description')
    list_filter = ('company', 'position_held', 'employment_contract')
    search_fields = ('company', 'position_held', 'employment_contract')

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('overview', 'name', 'backend_language', 'desktop_language', 'frontend_language', 'mobile_language', 'script_language', 'backend_framework', 'desktop_framework', 'frontend_framework', 'mobile_framework', 'script_framework', 'url', 'description')
    list_filter = ('name', 'backend_language', 'desktop_language', 'frontend_language', 'mobile_language', 'script_language', 'backend_framework', 'desktop_framework', 'frontend_framework', 'mobile_framework', 'script_framework')
    search_fields = ('name', 'backend_language', 'desktop_language', 'frontend_language', 'mobile_language', 'script_language', 'backend_framework', 'desktop_framework', 'frontend_framework', 'mobile_framework', 'script_framework')

@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('logo', 'name', 'knowledge_level')
    list_filter = ('name', )
    search_fields = ('name', )