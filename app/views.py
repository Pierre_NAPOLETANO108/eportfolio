from django.shortcuts import render
from django.views.generic.base import TemplateView
from app.models import Course, Background, Project, Skill

class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Page d\'accueil'
        return context

class CourseView(TemplateView):
    template_name = 'courses.html'
    model = Course

    def get_context_data(self, *args, **kwargs):
        context = super(CourseView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Mes Formations'
        context['courses'] = Course.objects.all()
        return context

class BackgroundView(TemplateView):
    template_name = 'backgrounds.html'
    model = Background

    def get_context_data(self, *args, **kwargs):
        context = super(BackgroundView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Mes Expériences Professionnelles'
        context['backgrounds'] = Background.objects.all()
        return context

class ProjectView(TemplateView):
    template_name = 'projects.html'
    model = Project

    def get_context_data(self, *args, **kwargs):
        context = super(ProjectView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Mes Projets'
        context['projects'] = Project.objects.all()
        return context

class SkillView(TemplateView):
    template_name = 'skills.html'
    model = Skill

    def get_context_data(self, *args, **kwargs):
        context = super(SkillView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Mes Compétences'
        context['skills'] = Skill.objects.all()
        return context

class ContactView(TemplateView):
    template_name = 'contact.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ContactView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Pierre NAPOLETANO - Page de contact'
        return context