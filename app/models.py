from django.db import models

class Organisation(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(verbose_name='Nom', max_length=50)
    city = models.CharField(verbose_name='Ville', max_length=50)
    zip_code = models.CharField(verbose_name='Code postal', max_length=50)
    address = models.TextField(verbose_name='Adresse')
    website_url = models.URLField(verbose_name='Site web')
    description = models.TextField(verbose_name='Description')

class School(Organisation):
    class Meta:
        verbose_name = 'Ecole'
        verbose_name_plural = 'Ecoles'

    logo = models.ImageField(verbose_name='Logo de l\'\xe9cole', upload_to='schools/logos/')
    status = models.CharField(verbose_name='Status', max_length=50)

    def __str__(self) -> str:
        return f'Ecole: {self.name}'

class Course(models.Model):
    class Meta:
        verbose_name = 'Formation'
        verbose_name_plural = 'Formations'

    school = models.ForeignKey(School, on_delete=models.DO_NOTHING)
    diploma = models.CharField(verbose_name='Diplome obtenu - pr\xe9par\xe9', max_length=50)
    start_date = models.DateField(verbose_name='D\xe9but de la formation')
    end_date = models.DateField(verbose_name='Fin de la formation')
    course_url = models.URLField(verbose_name='Page web de la formation')
    description = models.TextField(verbose_name='Description de la formation')

    def __str__(self) -> str:
        return f'Ecole: {self.school.name} - Diplome: {self.diploma}'

class Company(Organisation):
    class Meta:
        verbose_name = 'Entreprise'
        verbose_name_plural = 'Entreprises'

    logo = models.ImageField(verbose_name='Logo de l\'entreprise', upload_to='companies/logos/')
    activity = models.CharField(verbose_name='Activit\xe9 de l\'entreprise', max_length=50)

    def __str__(self) -> str:
        return f'Entreprise: {self.name}'

class Background(models.Model):
    class Meta:
        verbose_name = 'Expérience Professionnelle'
        verbose_name_plural = 'Expériences Professionnelles'
    
    class EmploymentContractChoices(models.TextChoices):
        SANDWICH_COURSES = ('alternance', 'Alternance')
        TEMPORARY_JOB = ('interim', 'Intérim')
        SEASONAL_EMPLOYMENT = ('saisonnier', 'Saisonnier')
        FIXED_TERM_CONTRACT = ('cdd', 'CDD')
        PERMANENT_CONTRACT = ('cdi', 'CDI')

    company = models.ForeignKey(Company, on_delete=models.DO_NOTHING)
    position_held = models.CharField(verbose_name='Poste occup\xe9', max_length=50)
    employment_contract = models.CharField(verbose_name='Contrat de travail', max_length=50, choices=EmploymentContractChoices.choices)
    start_date = models.DateField(verbose_name='D\xe9but de l\'exp\xe9rience')
    end_date = models.DateField(verbose_name='Fin de l\'exp\xe9rience')
    description = models.TextField(verbose_name='Description de l\'exp\xe9rience')

    def __str__(self) -> str:
        return f'Ecole: {self.school.name} - Diplome: {self.diploma}'

class Project(models.Model):
    class Meta:
        verbose_name = 'Projet'
        verbose_name_plural = 'Projets'
    
    overview = models.ImageField(verbose_name='Apercu du projet', upload_to='projects/overviews/')
    name = models.CharField(verbose_name='Nom du projet', max_length=50)
    backend_language = models.CharField(verbose_name='Langage backend', max_length=50, blank=True, null=True)
    desktop_language = models.CharField(verbose_name='Langage pour application de bureau', max_length=50, blank=True, null=True)
    frontend_language = models.CharField(verbose_name='Langage frontend', max_length=50, blank=True, null=True)
    mobile_language = models.CharField(verbose_name='Langage mobile', max_length=50, blank=True, null=True)
    script_language = models.CharField(verbose_name='Langage de script', max_length=50, blank=True, null=True)
    backend_framework = models.CharField(verbose_name='Framework backend', max_length=50, blank=True, null=True)
    desktop_framework = models.CharField(verbose_name='Framework pour application de bureau', max_length=50, blank=True, null=True)
    frontend_framework = models.CharField(verbose_name='Framework frontend', max_length=50, blank=True, null=True)
    mobile_framework = models.CharField(verbose_name='Framework mobile', max_length=50, blank=True, null=True)
    script_framework = models.CharField(verbose_name='Framework de script', max_length=50, blank=True, null=True)
    url = models.URLField(verbose_name='URL du projet', blank=True, null=True)
    description = models.TextField(verbose_name='Description du projet', blank=True, null=True)

    def __str__(self) -> str:
        return f'Projet: {self.name}'

class Skill(models.Model):
    class Meta:
        verbose_name = 'Competence'
        verbose_name_plural = 'Competences'

    logo = models.ImageField(verbose_name='Logo de la comp\xe9tence', upload_to='skills/logos/')
    name = models.CharField(verbose_name='Nom de la comp\xe9tence', max_length=50)
    knowledge_level = models.PositiveSmallIntegerField(verbose_name='Niveau de connaissance')

    def __str__(self) -> str:
        return f'Competence: {self.name}'